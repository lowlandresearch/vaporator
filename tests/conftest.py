from pathlib import Path
import os
import tempfile
import random

import pytest
import vaporator
from toolz import pipe
from toolz.curried import map

def random_file_name():
    return f'{bytes(random.sample(range(256), 16)).hex()}.txt'

@pytest.fixture(scope='session')
def dbx():
    return vaporator.dropbox.get_dropbox()

@pytest.fixture(scope='session')
def dbx_root(dbx):
    root = f'/vaporator_test_{os.urandom(12).hex()}'
    folder = dbx.files_create_folder_v2(root)
    yield folder.metadata
    dbx.files_delete_v2(root)

@pytest.fixture(scope='session')
def dbx_temp_file(dbx, dbx_root):
    content = os.urandom(2**10).hex().encode()
    return {
        'meta': dbx.files_upload(
            content, str(Path(dbx_root.path_display, random_file_name()))
        ),
        'content': content,
    }

PATHS = [
    'dir0/dir0/0.txt',
    'dir0/dir1/0.txt',
    'dir0/dir1/1.txt',
    'dir0/dir1/2.txt',
    'dir0/dir1/2.csv',
    'dir0/dir1/5.dat',
    # 'dir0/0.txt',
    # 'dir0/1.txt',
    # 'dir1/dir0/0.txt',
    # 'dir1/dir0/1.txt',
    # 'dir1/dir1/dir0/0.txt',
    # 'dir1/dir1/dir0/1.txt',
    # 'dir1/dir2/dir0/dir0/0.txt',
]

def get_paths(root):
    return pipe(
        PATHS,
        map(lambda p: Path(root, p)),
        tuple,
    )

CONTENT_SIZE = 16
def get_content(p):
    return (bytes(random.Random(str(p)).sample(range(256), CONTENT_SIZE))
            .hex()
            .encode())

@pytest.fixture(scope='session')
def temp_data():
    with tempfile.TemporaryDirectory(prefix='vaporator_test_') as root:
        paths = get_paths(root)
        content = {p: get_content(PATHS[i]) for i,p in enumerate(paths)}
        for i, path in enumerate(paths):
            path.parent.mkdir(parents=True, exist_ok=True)
            path.write_bytes(content[path])
        yield {'root': Path(root), 'paths': paths, 'content': content}

@pytest.fixture(scope='session')
def dbx_paths(dbx_root):
    return pipe(
        PATHS,
        map(lambda p: Path(dbx_root.path_display, p)),
        tuple,
    )

@pytest.fixture(scope='session')
def dbx_root_with_temp(dbx_root, dbx_paths, temp_data):
    root, paths, contents = [temp_data.get(k)
                             for k in ['root', 'paths', 'content']]
    dbx_root_path = Path(dbx_root.path_display)

    vaporator.dropbox.local_sync_folder(
        dbx, root, dbx_root_path,
    )

    return dbx_root
