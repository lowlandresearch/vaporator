from pathlib import Path

from toolz import pipe
from toolz.curried import map

import vaporator

def test_get_hash(dbx, dbx_temp_file):
    the_hash = vaporator.dropbox.get_hash(dbx_temp_file['content'])
    assert the_hash == dbx_temp_file['meta'].content_hash

def test_is_file(dbx_temp_file):
    assert vaporator.dropbox.is_file(dbx_temp_file['meta'])

def test_is_folder(dbx_root):
    assert vaporator.dropbox.is_folder(dbx_root)
    
def test_get_file_meta(dbx, dbx_temp_file):
    meta = vaporator.dropbox.get_file_meta(
        dbx, dbx_temp_file['meta'].path_display
    )
    assert meta.content_hash == dbx_temp_file['meta'].content_hash
        
    
