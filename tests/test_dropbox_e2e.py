from pathlib import Path

from toolz import pipe, curry
from toolz.curried import map

import vaporator

def test_local_sync_folder(dbx, dbx_root, dbx_paths, temp_data):
    root, paths, contents = [temp_data.get(k)
                             for k in ['root', 'paths', 'content']]
    dbx_root_path = Path(dbx_root.path_display)

    vaporator.dropbox.local_sync_folder(
        dbx, temp_data['root'], dbx_root_path,
    )

    get_meta = vaporator.dropbox.get_file_meta(dbx)
    for i, meta in enumerate([get_meta(p) for p in dbx_paths]):
        content = contents[paths[i]]
        print(content, paths[i], paths[i].read_text(), dbx_paths[i])
        assert vaporator.dropbox.get_hash(content) == meta.content_hash


def double_path(path):
    return Path(str(path) + '-double.txt')

def double_content(content):
    return content + b'\n\n' + content

@curry
def double_txt(dbx, meta, content):
    new_path = double_path(Path(meta.path_display))
    return dbx.files_upload(double_content(content), str(new_path))

def test_dbx_walk_folder(dbx, dbx_root, dbx_paths, temp_data):
    root, paths, contents = [temp_data.get(k)
                             for k in ['root', 'paths', 'content']]
    
    vaporator.dropbox.dbx_walk_folder(
        dbx, dbx_root.path_display,
        file_transform=double_txt(dbx),
    )

    for i, path in enumerate(paths):
        new_path = double_path(dbx_paths[i])
        meta = vaporator.dropbox.get_file_meta(dbx, new_path)
        new_hash = vaporator.dropbox.get_hash(double_content(contents[path]))
        assert meta.content_hash == new_hash
