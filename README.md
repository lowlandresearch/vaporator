# vaporator

Tools for transforming scientific data in the cloud.

Currently works with:

- Dropbox

## Requirements

- Python3.7+ only (uses `dataclass`)

## Installation

```
pip install vaporator
```

Modify your `~/.tokenmanager.yml` file to contain the following
token data:

```
dropbox:
  token: <your dropbox API token>
```

You can just paste the above at the bottom of the file (with your
actual token data, of course).

## Usage

```python
import vaporator

def jws_transformer(meta, data):
    pass

box = vaporator.dropbox.get_databox('/your/root/directory')
box.transform(jws_transformer)

```

## Caveats

Currently, `vaporator` is only capable of the following:

- Working with Dropbox
- Transforming Jasco `*.jws` files (via `jascobin`)
