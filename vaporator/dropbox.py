import hashlib
import concurrent.futures
from pathlib import Path
import logging
import os
from typing import Union

import dropbox
from dropbox.files import (
    FileMetadata, FolderMetadata, ListFolderResult,
)
from toolz import pipe, curry
from toolz.curried import (
    map, filter, partition_all, concat,
)
from multipledispatch import dispatch
import tokenmanager
from larcutils.common import vmap

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

DROPBOX_HASH_CHUNK_SIZE = 4 * 2**20

def is_folder(meta):
    return type(meta) is FolderMetadata

def is_file(meta):
    return type(meta) is FileMetadata

@dispatch(bytes)
def get_hash(content: bytes):
    return pipe(
        content,
        partition_all(DROPBOX_HASH_CHUNK_SIZE),
        map(bytes),
        map(lambda chunk: hashlib.sha256(chunk).digest()),
        concat, bytes,
        hashlib.sha256,
        lambda h: h.hexdigest(),
    )

@dispatch(Path)                 # noqa
def get_hash(path: Path):
    return pipe(
        path.read_bytes(),
        get_hash,
    )

@curry
def get_file_meta(dbx: dropbox.Dropbox, path: Path):
    try:
        meta = dbx.files_get_metadata(str(path))
    except dropbox.exceptions.ApiError as error:
        if not error.error.get_path().is_not_found():
            log.error(f'{error}')
        meta = None
    return meta

def only_files(folder: ListFolderResult):
    return pipe(
        folder.entries,
        filter(is_file),
        tuple,
    )

def only_folders(folder: ListFolderResult):
    return pipe(
        folder.entries,
        filter(is_folder),
        tuple,
    )

file_or_folder = Union[FileMetadata, FolderMetadata]

@curry
def only_ext(ext: str, meta: FileMetadata):
    return Path(meta.path_lower).suffix == ext

@curry
def transform_file(dbx: dropbox.Dropbox, transform, file_meta):
    _, response = dbx.files_download(file_meta.path_lower)
    if response.status_code == 200:
        return transform(file_meta, response.content)
    else:
        raise IOError(f'Got status code {response.status_code}:\n'
                      f'{response.content}')

@curry
def transform_folder(dbx, transform, file_meta):
    pass

# N_WORKERS = min(10, multiprocessing.cpu_count() * 5)

N_WORKERS = 5                   # Dropbox RateLimit grrrrr

@curry
def dbx_walk_folder(dbx: dropbox.Dropbox,
                    root: str, *,
                    file_filter=lambda meta: True,
                    file_transform=None,
                    folder_filter=lambda meta: True,
                    folder_transform=None):
    file_transform = (transform_file(dbx, file_transform)
                      if file_transform else None)

    folder_transform = (transform_folder(dbx, folder_transform)
                        if folder_transform else None)

    stack = [(root, dbx.files_list_folder(root))]

    with concurrent.futures.ThreadPoolExecutor(
            max_workers=N_WORKERS) as executor:
        future_to_meta = {}
        while stack:
            path, folder = stack.pop()
            if file_transform:
                for meta in pipe(folder, only_files, filter(file_filter)):
                    future = executor.submit(file_transform, meta)
                    future_to_meta[future] = meta
            if folder_transform:
                future = executor.submit(folder_transform, folder)
                future_to_meta[future] = folder

            stack.extend(pipe(
                only_folders(folder),
                filter(folder_filter),
                map(lambda f: (
                    f.path_display, dbx.files_list_folder(f.path_lower)
                )),
            ))

        for future in concurrent.futures.as_completed(future_to_meta):
            meta = future_to_meta[future]
            try:
                data = future.result()
            except Exception as exc:
                log.error(f'ERROR: {meta.name} generated an'
                          f' exception: \n{exc}')
            else:
                log.info(f'{meta.name} generated {data}')

@curry
def sync_local_dbx(dbx: dropbox.Dropbox,
                   local_path: Path, dbx_path: Path):
    log.info(f'Reading content of {local_path}')
    meta = get_file_meta(dbx, dbx_path)
    needs_upload = True
    write_mode = dropbox.files.WriteMode.add
    content = local_path.read_bytes()
    if meta:
        content_hash = get_hash(content)
        if content_hash == meta.content_hash:
            needs_upload = False
            log.info(f'  {local_path} is unchanged')
        else:
            log.info(f'  {local_path} has CHANGED.. Needs re-upload')
            write_mode = dropbox.files.WriteMode.overwrite
    else:
        log.info(f'  {dbx_path} does not exist')

    if needs_upload:
        log.info(f'Doing UPLOAD of {local_path} to {dbx_path}')
        dbx.files_upload(content, str(dbx_path), write_mode)

@curry
def get_dbx_path(local_root, dbx_root, local_path):
    nparts = len(Path(local_root).expanduser().resolve().parts)
    return Path(dbx_root, *Path(local_path).parts[nparts:])

@curry
def get_dbx_paths(local_root, dbx_root, local_paths):
    return pipe(
        local_paths,
        map(get_dbx_path(local_root, dbx_root)),
        tuple,
    )

@curry
def local_sync_folder(dbx: dropbox.Dropbox,
                      local_root: (str, Path),
                      dbx_root: (str, Path), *,
                      file_filter=lambda path: True,
                      folder_filter=lambda path: True,
                      dry_run: bool=False):
    @curry
    def log_file_filter(path):
        keep = file_filter(path)
        if not keep:
            log.info(f'FILTERED file: {path}')
        return keep

    @curry
    def log_folder_filter(path):
        keep = folder_filter(path)
        if not keep:
            log.info(f'FILTERED folder: {path}')
        return keep

    def filter_dirs(root, dirs):
        # side-effects... bleh
        paths = [Path(root, d).resolve() for d in dirs]

        to_keep = pipe(
            enumerate(paths),
            filter(lambda i_d: log_folder_filter(i_d[-1])),
            vmap(lambda i, d: i),
            set,
        )

        to_delete = sorted(set(range(len(dirs))) - to_keep)

        for i in reversed(to_delete):
            del dirs[i]
        
    log.info('Starting walk of local folder')
    paths = []
    for root, dirs, files in os.walk(local_root, topdown=True):
        filter_dirs(root, dirs)
        paths.extend(pipe(
            [Path(root, f).resolve() for f in files],
            filter(log_file_filter),
        ))
    paths.sort()
    log.info('  ...done')
        
    dbx_paths = get_dbx_paths(local_root, dbx_root, paths)

    def fake_sync(*args):
        log.info(f'DRY-RUN: {args}')

    def vcall(f):
        def call(args):
            return f(*args)
        return call

    with concurrent.futures.ThreadPoolExecutor(
            max_workers=N_WORKERS) as executor:
        future_to_paths = {}
        for path, dbx_path in zip(paths, dbx_paths):
            future = executor.submit(
                fake_sync if dry_run else sync_local_dbx,
                dbx, path, dbx_path,
            )
            future_to_paths[future] = (path, dbx_path)

        for future in concurrent.futures.as_completed(future_to_paths):
            path, dbx_path = future_to_paths[future]
            try:
                meta = future.result()
            except Exception as exc:
                log.error(f'ERROR: {path} generated an exception: \n{exc}')
            else:
                log.info(f'{path} generated {meta}')

@dispatch()
def get_dropbox():
    return get_dropbox(tokenmanager.get_tokens().dropbox.token)

@dispatch(str)                  # noqa
def get_dropbox(token: str):
    session = dropbox.create_session(max_connections=N_WORKERS)
    return dropbox.Dropbox(token, session=session)
